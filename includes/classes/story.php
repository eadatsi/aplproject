<?php
include_once 'connection.php';

class Story{
  /*Fetch all available stories*/
  public function fetch_all(){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users");
    $query->execute();
    return $query->fetchAll();
  }

/*Get all stories with a specific tag*/
  public function getStorybyTag($tag){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM stories WHERE story_tag = ?");
    $query->bindValue(1,$tag);
    $query->execute();
    return $query->fetchAll();
  }

/*Upload a story*/
  public function addStory($title, $body, $tags){
    global $pdo;
    $date = date("Y-m-d");
    $query = $pdo->prepare("INSERT INTO stories (story_title, story_body, story_date, story_tag) VALUES (?,?,?,?)")
    $query->bindValue(1,$title);
    $query->bindValue(2,$body);
    $query->bindValue(3,$date);
    $query->bindValue(4,$tag);
    $query->execute();
  }

/*Delete a Story*/
  public function deleteStory($id){
    global $pdo;
    $query = $pdo->prepare("DELETE FROM stories WHERE story_id = ?");
    $query->bindValue(1,$id);
    $query->execute();
  }
}
?>
