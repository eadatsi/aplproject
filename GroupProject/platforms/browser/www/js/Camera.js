

//  Variable for picture source and return value format.
var pictureSource;
var destinationType;

// Loading device API libraries.
document.addEventListener("deviceready",onDeviceReady,false);

// device APIs are ready to use.
function onDeviceReady() {
pictureSource=navigator.camera.PictureSourceType;
destinationType=navigator.camera.DestinationType;
}

// The function is called on successful retrieval of photo.
function onPhotoDataSuccess(imageData) {

	var smallImage = document.getElementById('blog_img');

	// This function is used for unhide the image elements
	// smallImage.style.display = 'block';
	var text_post = $('#text-post').val();
	var author = $('#name').val();
	if (author == '')
	{ 
	    author = 'Anon'
	}
	
	var textnode = document.createTextNode(text_post + '              - ' + author );

	// Trying something here !
	// window.resolveLocalFileSystemURL(imgUri, function success(fileEntry) { 

	// 	console.log("got file: " + fileEntry.fullPath);
	// 	$.ajax({
	// 	    url     : 'http://cs.ashesi.edu.gh/~joseph.brown-pobee/includes/insert_post.php',
	// 	    type: "POST",
	// 	    data: {
	// 	    	submit : 'submit',
	// 	        post_body: '',
	// 	        post_name: '',
	// 	        post_image : fileEntry.fullPath
	// 	      },
	// 	    dataType: 'json',

	// 	    success: function (data) {
	// 	        console.log('Any data ' + data);
	// 	    }
	// 	});


	// });
	

	var node = document.createElement("P");
	var image = document.createElement("IMG");
	image.setAttribute("src", imageData)
	var imagePath = image.src;

	if (text_post != ''){
		node.appendChild(image);
		node.appendChild(textnode);
		document.getElementById('post-div').appendChild(node);

		$.ajax({
		    url     : 'http://cs.ashesi.edu.gh/~joseph.brown-pobee/includes/insert_post.php',
		    type: "POST",
		    data: {
		    	submit : 'submit',
		        post_body: text_post,
		        post_name: author,
		        post_image : imagePath
		      },
		    dataType: 'json',

		    success: function (data) {
		        console.log('Any data ' + data);
		    }
		});
	}

// This function is used to display the captured image
// smallImage.src = "data:image/jpeg;base64," + imageData;

}

// This function is called on the successful retrival of image.
function onPhotoURISuccess(imageURI,img) {
var largeImage = document.getElementById(img);

// This function is used for unhiding the image elements
largeImage.style.display = 'block';

// This function is used to display the captured image.
largeImage.src = imageURI;

}

// This function will execute on button click.
function capturePhoto() {
// Take picture using device camera and retrieve image as base64-encoded string

navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
destinationType: destinationType.FILE_URL });

}

// This function will execute on button click.
function capturePhotoEdit() {
// Take picture using device camera, allow edit, and retrieve image as base64-encoded string
navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
destinationType: destinationType.DATA_URL });
}

// This function will execute on button click.
function getPhoto(source) {
// Retrieve image file location from specified source
navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
destinationType: destinationType.FILE_URI,
sourceType: source });
}

// This function will be called if some thing goes wrong.
function onFail(message) {
alert('Failed because: ' + message);
}
