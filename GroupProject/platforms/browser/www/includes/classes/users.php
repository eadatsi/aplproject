<?php
include_once 'connection.php';

class User{
  /*Fetch all Registered Users*/
  public function fetch_all(){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users");
    $query->execute();
    return $query->fetchAll();
  }

/*Get a User by their first and last names*/
  public function getUserByName($fname,$lname){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users WHERE first_name = ? AND last_name = ?");
    $query->bindValue(1,strtolower($fname));
    $query->bindValue(2,strtolower($lname));
    $query->execute();
    return $query->fetch();
  }

/*Get a User by their ID*/
  public function getUserByID($id){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users WHERE user_id = ?");
    $query->bindValue(1,$id);
    $query->execute();
    return $query->fetch();
  }

/*Create a New User*/
  public function createUser($username, $f_name,$l_name,$email, $password, $gender, $dob, $number){
    global $pdo;
    $query = $pdo->prepare("INSERT INTO users (username, first_name, last_name, email, password, gender, date_of_birth, phone_number) VALUES(?,?,?,?,?,?,?,?)");
    $query->bindValue(1,$username);
    $query->bindValue(2,$f_name);
    $query->bindValue(3,$l_name);
    $query->bindValue(4,$email);
    $query->bindValue(5,$password);
    $query->bindValue(6,$gender);
    $query->bindValue(7,$dob);
    $query->bindValue(8,$number);
    $query->execute();
  }

/*Delete a User*/
  public function deleteUser($id){
    global $pdo;
    $query = $pdo->prepare("DELETE FROM users WHERE user_id = ?");
    $query->bindValue(1,$id);
    $query->execute();
  }

/*Used when Logging in to Validate credentials*/
  public function validateUser($username,$password){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users WHERE username = ?");
    $query->bindValue(1,$username);
    $query->execute();
    $result = $query->fetch();
    if(password_verify($password, $result['password'])){
      return true;
    }
    else{
      return false;
    }
  }

/*Checks during registration if a username already exists*/
  public function check_username($username){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users WHERE username = ?");
    $query->bindValue(1,$username);
    $query->execute();
    $result = $query->fetch();
    return $result;
/*    if($result = mysqli_query($this->connect(), $sql)){
      $rowcount = mysqli_num_rows($result);
    }
    if($rowcount>0){
      return "Username already exists";
    }*/
  }

/*Checks during registration if an email already exists*/
  public function check_email($email){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM users WHERE email = ?");
    $query->bindValue(1,$email);
    $query->execute();
    $result = $query->fetch();
    return $result;
  }
}
  ?>
