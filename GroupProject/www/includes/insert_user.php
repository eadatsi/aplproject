<?php

include 'classes/users.php';
$user = new User;
$error = ""; // Initialize error as blank

$fname_error = '';
$lname_error = '';
$user_error = '';
$email_error = '';
$password_error = '';
$confirm_password_error = '';
$phone_error = '';
$gender_error = '';
$dob_error = '';

if(isset($_POST['submit'])){
  $f_name = $_POST['f_name']; //The first name field data
  $l_name = $_POST['l_name']; //The last name field data
  $username = $_POST['username'];
  $email = $_POST['email']; //The email field data
  $password_raw = $_POST['password']; //The password field data
  $con_password_raw = $_POST['confirm_password']; //The confirm password field data
  $gender = $_POST['gender']; //The gender field data
  $date_of_birth = $_POST['DOB']; //The date of birth field data
  $phone_number = $_POST['phone']; //The phone field data

  /*================Validations==================*/
  # Validate First Name and Last Name #
  // if its not alpha numeric, throw error
  if (empty($f_name) or empty($l_name)) {
    $user_error= 'Please enter tell us your name.';
    $error .= $user_error;
  }

  //If it has wierd characters, throw error

  # Validate Username #
  // if its not alpha numeric, throw error
  if (!ctype_alnum($username)) {
    $user_error= 'Username should be alpha numeric characters only.';
    $error .= '<p class="error">Username should be alpha numeric characters only.</p>';
  }

  if($user->check_username($username)==1){
    $user_error = 'Username already exists';
    $error .= 'Username already exists';
  }

  // if username is not 3-20 characters long, throw error
		if (strlen($username) < 3 OR strlen($username) > 20) {
			$user_error= 'Username should be within 3-20 characters long.';
      $error .= '<p class="error">Username should be within 3-20 characters long.</p>';
		}

  # Validate Password #
		// password is not 3-20 characters long, throw error
		if (strlen($password_raw) < 3 OR strlen($password_raw) > 20) {
      $password_error = 'Password should be within 3-20 characters long.';
      $error .= '<p class="error">Password should be within 3-20 characters long.</p>';
		}

	# Validate Confirm Password #
		// if Password does not match confirm password, throw error
		if ($con_password_raw != $password_raw) {
      $confirm_password_error = 'Confirm password mismatch';
      $error .= '<p class="error">Confirm password mismatch.</p>';
		}

    # Validate Email #
		// if email is invalid, throw error
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // you can also use regex to do same
      $email_error = 'Enter a valid email address.';
      $error .= '<p class="error">Enter a valid email address.</p>';
		}

    if($user->check_email($email)==1){
      $email_error = 'Email already exists';
    }

    # Validate Phone #
		// if phone is invalid, throw error
		if (!ctype_digit($phone_number) OR strlen($phone_number) != 10) {
      $phone_error = 'Enter a valid phone number.';
      $error .= '<p class="error">Enter a valid phone number.</p>';
	}

  # Validate Gender #
  if ($gender == '0') {
      $gender_error = 'Please select a gender';
      $error .= '<p class="error">Enter city.</p>';
		}

    # Validate Date of Birth #
    $today = date("Y-m-d");
    $test_arr = explode('-', $date_of_birth);
    $arr = explode('-',$today);
    if(count($test_arr)==3){
    if (checkdate($test_arr[1], $test_arr[2], $test_arr[0])){
    }
    else{
      $dob_error = "Enter a valid date";
      $error .= $dob_error;
    }
    if($test_arr[0] > $arr[0]){
      $dob_error = "You cannot be from the future";
      $error .= $dob_error;
    }
    else{
      if ($test_arr[1] > $arr[1] && $test_arr[0] == $arr[0]) {
      $dob_error = "You cannot be from the future";
      $error.= $dob_error;
    }
    else{
      if ($test_arr[2] > $arr[2] && $test_arr[1] == $arr[1] & $test_arr[0] == $arr[0]){
        $dob_error = "You cannot be from the future";
        $error .= $dob_error;
      }
    }
  }
}else{
  $dob_error = "Enter a date";
  $error .= $dob_error;
}
if(empty($error)){
  $password = PASSWORD_HASH($password_raw, PASSWORD_DEFAULT);
  $user->createUser($username, $f_name, $l_name, $email, $password, $gender, $date_of_birth, $phone_number);
  echo "Inserted";
}
else{
  echo $error;
}
}
/*
VALIDATIONS TO ADD:
PASSWORD SHOULD HAVE UPPERCASE, LOWERCASE AND SPECIAL CHARACTER
FIRST NAME AND LAST NAME CANNOT HAVE WIERD CHARACTERS
*/

?>
