<?php
include_once 'connection.php';

class Team{
  /*Fetch all Available Teams*/
  public function fetch_all(){
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM teams");
    $query->execute();
    return $query->fetchAll();
  }

/*Add a team to the database*/
  public function addTeam($team_name, $team_logo, $coach_name, $date_founded){
    global $pdo;
    $query = $pdo->prepare("INSERT INTO teams (team_name, team_logo, coach_name, date_founded) VALUES(?,?,?,?)");
    $query->bindValue(1,$team_name);
    $query->bindValue(2,$team_logo); //should be image source
    $query->bindValue(3,$coach_name);
    $query->bindValue(4,$date_founded);//should be date
  }

/*Update the Team Stats of a given team*/
  public function updateTeamStats($team_name, $wins, $draws, $losses, $gf, $clean_sheets, $ga, $own_goals, $yellow, $red, $tackles){
    global $pdo;
    $query = $pdo->prepare("UPDATE teams SET wins = ?, draws = ?, losses = ?, goals_scored = ?, clean_sheets = ?, goals_conceded = ?, own_goals =?, yellow_cards = ?, red_cards = ?, tackles = ?");
    $query->bindValue(1,$wins);
    $query->bindValue(2,$draws);
    $query->bindValue(3,$losses);
    $query->bindValue(4,$gf);
    $query->bindValue(5,$clean_sheets);
    $query->bindValue(6,$ga);
    $query->bindValue(7,$own_goals);
    $query->bindValue(8,$yellow);
    $query->bindValue(9,$red);
    $query->bindValue(10,$tackles);
    $query->execute();
  }
}
 ?>
